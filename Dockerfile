FROM ubuntu:18.04

RUN apt-get update && \
    apt-get install -y make gcc-avr binutils-avr avr-libc gcc g++ && \
    rm -rf /var/lib/apt/lists/*

SHELL ["/bin/bash", "-c"]

CMD ["/bin/bash"]
